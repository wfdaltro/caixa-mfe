import  Keycloak, { KeycloakConfig } from 'keycloak-js'

export {emitEvent, listenEvent} from './events'

export {clearSession, getSessionItem, removeSessionItem, setSessionItem} from './session-storage'

var keycloak =  Keycloak({
    url: 'http://localhost:28080/auth',
    realm: 'poc',
    clientId: 'angular'
});

 
keycloak.init({ onLoad: 'check-sso' }).then(function () {
    if (keycloak.authenticated) {
        sessionStorage.setItem('token', keycloak.token);
        sessionStorage.setItem('tokenParsed', JSON.stringify(keycloak.tokenParsed));
       keycloak.loadUserProfile();
    } else {
        keycloak.login();
    }
});

export function logoff() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('tokenParsed');
    keycloak.logout();
}

export function getUserInfo() {
    const idTokenParsed = JSON.parse(sessionStorage.getItem('tokenParsed'));
    console.log(idTokenParsed)
    return {
        username:  idTokenParsed['preferred_username'],
        email:   idTokenParsed['email'],
        name: idTokenParsed['name'],
        givenName:  idTokenParsed['given_name'],
        familyName: idTokenParsed['family_name']
    };
}
