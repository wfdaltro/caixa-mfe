 
    export function setSessionItem(key: string, value: any) {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

    export function getSessionItem(key: string) {
        return JSON.parse(sessionStorage.getItem(key) || '{}');
    }

    export function removeSessionItem(key: string) {
        sessionStorage.removeItem(key);
    }

    export function clearSession(){
        sessionStorage.clear();
    }


