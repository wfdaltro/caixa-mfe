const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-ts");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "cef",
    projectName: "styleguide",
    webpackConfigEnv,
    argv,
    rules:[
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader" },
     
      {
        test: /\.(eot|ttf|woff2?|otf|svg|png|jpg)$/,
        loaders: ['file']
    }
    ]
    
  });

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
  });
};
