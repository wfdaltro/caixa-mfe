import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
//@ts-ignore
//import {emitEvent, autenticated} from '@cef/core';

@Injectable({ providedIn:'root' })

export class AuthGuard implements CanActivate{ 

    constructor (private router: Router) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        
        return true;
    }

  
}
