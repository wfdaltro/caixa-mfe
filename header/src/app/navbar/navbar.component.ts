import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

//@ts-ignore
import {emitEvent, logoff, getUserInfo} from '@cef/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public appName: string = " ";

  public user : any;

  constructor() { }

  ngOnInit(): void {
    this.user = getUserInfo();
  }

  public setApplicationName(name: string): void{
    this.appName = name;
    emitEvent("teste", {nome: name});
  }


  public logout(){
    logoff();
  }

 
}
