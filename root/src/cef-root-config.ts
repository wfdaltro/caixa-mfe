import { registerApplication, start } from "single-spa";
import {environment} from './environments/sso-dev'

//@ts-ignore
import {autenticated, login, listenEvent} from '@cef/core';

import {
  constructApplications,
  constructRoutes,
  constructLayoutEngine,
} from "single-spa-layout";
import microfrontendLayout from "./microfrontend-layout.html";

const routes = constructRoutes(microfrontendLayout);
const applications = constructApplications({
  routes,
  loadApp({ name }) {
    return System.import(name);
  },
});
const layoutEngine = constructLayoutEngine({ routes, applications });

applications.forEach(registerApplication);
layoutEngine.activate();
start();

listenEvent(
  'teste',
  (event) => {
    console.log('portal-angular-product', event);
  }
);

listenEvent(
  'nao_Autenticado',
  (event) => {
    console.log('Tem que chamar o keycloak agora', event);
    alert("eeeo de guarda");
    login(environment);
  }
);

window.onload = function () {
  if(!autenticated()){
    alert("Load");
    login(environment);
  }
}